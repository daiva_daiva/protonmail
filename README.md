# PROTONMAIL

****Automatizuoto testavimo užduoties sprendimo Demo versija****

protonTestSuite is demo version for automatical testing of ProtonMail software. ProtonMail uses a flexible hybrid system of  folders and labels to maximize productivity and
organization within your inbox. The difference between the two is that folders allow users to
store a message within them while labels are applied on top of a message. protonTestSuite test suite is limited to the test cases for testing settings section located at
https://beta.protonmail.com/settings/labels. 
Selenium tests generator Katalon automation recorder (https://chrome.google.com/webstore/detail/katalon-recorder-selenium/ljdobmomdgdljniojadhoplhkpialdid) was used to develop protonTestSuite tests. It is Selenium IDE alternative to record and export Selenium scripts (with reports & screenshots). Katalon Recorder supports the legacy Selenium IDE's commands and extension scripts (AKA user-extensions.js) for developing custom locator builders and actions.
Katalon Automation Recorder extension (https://docs.katalon.com/katalon-studio/docs/katalon-addon-for-chrome.html ) is required to capture objects in Active Browser. To install the Katalon Automation Recorder extension for Chrome click on this link: https://chrome.google.com/webstore/detail/katalon-recorder-selenium/ljdobmomdgdljniojadhoplhkpialdid .

protonTestSuite is presented as protonTestSuite.html. protonTestSuite automated tests must be run using Chrome browser. To run scrips from protonTestSuit, You need to import test suite first:
1.	Press Customize and control Google chrome button, choose menu More tools->Extensions and make sure that Katalon Recorder (Selenium tests generator) extension is enabled;
2.	Run Katalon Recorder (Selenium tests generator), pressing Katalon Recorder icon presented near the right upper corner;
3.	In Katalon Recorder, test suits’ explorer part, choose “Open test suit” icon (it is displayed as folder icon) – after Browse window is displayed, choose protonTestSuite.html file and push button Open.  protonTestSuite test cases should be imported.
 
protonTestSuite test suite is a demo version consisting of two test cases:
1.	Naujos label pridejimas – test case script automatically tests creation of a new label. It opens https://mail.protonmail.com/login page, automatically logs in and tries to create new label pushing ADD LABEL button. The name of a new label is hardcoded in this demo version, but later script versions will implement passing the label’s name as a parameter. If a label with the name hardcoded already exists screenshot is made and new label is not added. If a label with the name hardcoded doesn’t exist new label is added.  

2.	Label panaikinimas- test case script automatically tests deletion of the first label existing in FOLDERS/LABELS folder. It opens https://mail.protonmail.com/login page, automatically logs in and tries to delete the label, automatically pushing DELETE button presented near the label. After DELETE confirmation it removes label from the list. In the current ProtonMail version You have no labels.  text is displayed in case if there is no one label in the FOLDERS/LABELS folder. Script looks for this text in the page automatically accessed through menu LABELS/FOLDERS: if it is found, script doesn’t try to delete label, and ends. 

Note 1. A lot of pauses are used in the script for more convenient review of scripts‘performance.  
Note 2. Both positive and negative cases are tested by every test case script. As there are a lot of features must be tested, more cases should be written in the future versions. 

To run protonTestSuite test case:
1.	Press Customize and control Google chrome button, choose menu More tools->Extensions and make sure that Katalon Recorder (Selenium tests generator) extension is enabled;
2.	Run Katalon Recorder (Selenium tests generator), pressing Katalon Recorder icon presented near the right upper corner;
3.	Select test case You want to run;
4.	Press Play icon in the Katalon recorder’s toolbar:
5.	After Chrome browser opens, it is possible to observe automatically testing performance;
6.	Logs, screenshots and other information is available at the lower part of the main Katalon recorder window.

For use of Giltab CI pipeline:
1.	Katalon recorder test scripts should be exported to format supported by Katalon Studio;
2.	Katalon recorder must be installed from: https://www.katalon.com/ ;
3.	Katalon studio should be integrated with Gitlab: https://docs.katalon.com/katalon-studio/videos/git-integration.html . Git is a distributed version control system, which means you can work locally but you can also share or "push" your changes to other servers. Before you can push your changes to a GitLab server you need a secure communication channel for sharing information. The SSH protocol provides this security and allows you to authenticate to the GitLab remote server without supplying your username or password each time. For that purpose, in Gitlab, add an SSH key:  https://gitlab.com/profile/keys ;
4.	Install and register (https://docs.gitlab.com/runner/register/index.html) Runner(s) for this project. Runners are processes that pick up and execute jobs for GitLab. You can set up as many Runners as you need to run your jobs. A Runner that is specific only runs for the specified project(s). A shared Runner can run jobs for every project that has enabled the option Allow shared Runners under Settings > CI/CD. Projects with high demand of CI activity can also benefit from using specific Runners. By having dedicated Runners you are guaranteed that the Runner is not being held up by another project’s jobs. You can set up a specific Runner to be used by multiple projects. The difference with a shared Runner is that you have to enable each project explicitly for the Runner to be able to run its jobs. Runners can be placed on separate users, servers, and even on your local machine. Specify the following URL during the Runner setup: https://gitlab.com/ . Use registration token during setup of the runner. Follow the instructions as described in https://docs.gitlab.com/runner/install/ ; https://gitlab.com/daiva_daiva/dg_ci/-/settings/ci_cd#js-runners-settings . 
4.	After installing a runner as a service, start it. You can either run the service using the Built-in System Account (recommended) or using a user account. To start runner for Windows OS,  read https://docs.gitlab.com/runner/install/windows.html ; https://docs.microsoft.com/en-us/powershell/scripting/getting-started/starting-windows-powershell?view=powershell-6#with-administrative-privileges-run-as-administrator .
5.	For using Giltab CI pipeline You need to create configuration file gitlab-ci.yml using a YAML within each project (read https://docs.gitlab.com/ee/ci/README.html ). In this file, you must define the scripts you want to run, define include and cache dependencies, choose commands you want to run in sequence and those you want to run in parallel, define where you want to deploy your app, and specify whether you will want to run the scripts automatically or trigger any of them manually. Once you’re familiar with GitLab CI/CD you can add more advanced steps into the configuration file. To add scripts to that file, you’ll need to organize them in a sequence that suits your application and are in accordance with the tests you wish to perform. Once you’ve added your .gitlab-ci.yml configuration file to your repository, GitLab will detect it and run your scripts with the tool called GitLab Runner, which works similarly to your terminal (read 4., 5. in this section). The scripts are grouped into jobs, and together they compose a pipeline. GitLab CI/CD not only executes the jobs you’ve set but also shows you what’s happening during execution, as you would see in your terminal. For details read:  https://docs.gitlab.com/ee/ci/yaml/README.html .
6.	For more details read : https://docs.gitlab.com/ee/ci/README.html ; https://docs.gitlab.com/ee/topics/autodevops/ ; https://docs.gitlab.com/ee/ci/introduction/ ; https://testingbot.com/support/other/gitlab-ci .

You can find the examples here: https://dzone.com/articles/continuous-integration-with-gitlab ; https://www.youtube.com/watch?v=1iXFbchozdY ; https://www.youtube.com/watch?v=pBe4t1CD8Fc ; https://gitlab.com/psych0der/hourglass/-/blob/master/.gitlab-ci.yml .

















